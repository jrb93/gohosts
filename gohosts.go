package main

import (
	"os"

	"io"

	"bufio"
	"fmt"

	log "github.com/sirupsen/logrus"
)

func main() {
	hostsfile := "C:\\Windows\\System32\\drivers\\etc\\hosts"
	f, err := os.OpenFile(hostsfile, os.O_RDWR, 0755)
	if err != nil {
		log.WithFields(log.Fields{
			"file": f.Name(),
		}).Error("File failed to open")
	}
	defer fileClose(f)

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}
	if err := scanner.Err(); err != nil {
		// TODO: Improve Error
		log.Errorf("Reading file: %s", err)
	}
}

func fileClose(c io.Closer) {
	err := c.Close()
	if err != nil {
		log.WithFields(log.Fields{
			"file": c,
		}).Error("File failed to close")
	}
}
